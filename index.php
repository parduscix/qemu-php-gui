<?php 
$user="";			// This is user value
$passwd="";			// This is password value
$folder="";			// This is falder value (Example: /home/user/imgs/)
$maxcore=exec("nproc");
if (isset($_GET["cmd"])){
$cmd=$_GET["cmd"];
	if($cmd == "logout"){
		setcookie("passwd","");
		setcookie("user","");
		echo '<a href="./index.php">login</a>';
		exit;
	}
	if($cmd=="killall"){
		shell_exec("killall qemu-system-x86_64 2> /dev/null | cat > /dev/null &");
		echo 'All VM is Killed<br>';
	}
	if($cmd=="list"){
		echo "<p><b>VM list:<b></p>";
		echo "<TABLE border=1 width=\"%25\" >";
		system("ps ax | grep qemu | grep -v grep | sed \"s|^|<TR><TD>|g\" | sed \"s|$|</TR></TD>|g\"");
		echo "</TABLE>";
		echo '
<form action="/index.php?cmd=kill" method="post" autocomplete="on">
	PID:&nbsp<input type="text" name="name" value="" >
	<input type="submit" value="Kill" >
</form>
		';
		exit;
	}
	if($cmd=="listimage"){
		echo "<p><b>List disk images:<b>".$folder."</p>";
		echo "<TABLE border=1 width=\"%25\" >";
		system("ls -l".$folder." | grep img | sed \"s|^|<TR><TD>|g\" | sed \"s|$|</TR></TD>|g\"");
		echo "</TABLE>";
	echo '
<form action="/index.php?cmd=delete" method="post" autocomplete="on">
	Name:&nbsp<input type="text" name="name" value="" >.img
	<input type="submit" value="Delete" >
</form>
		';
		exit;
	}
}
if (($_COOKIE['passwd'] == $passwd) AND ($_COOKIE['user'] == $user)){
	if ($_COOKIE['user'] != ""){
		echo 'Login from:<b>'.$_COOKIE['user'].'</b>&nbsp&nbsp&nbsp<a href="./index.php?cmd=logout">Logout</a>';
	}
	if($cmd=="create"){
		echo '
<form action="/index.php?cmd=create" method="post" autocomplete="on">
	Name:&nbsp<input type="text" name="name" value="" >.img
	Size:&nbsp<input type="text" name="size" value="" >Gb
	<br><input type="submit" value="Cleate" >
</form>
		';
		if(isset($_POST['name']) AND isset($_POST['size'])){
			if(is_numeric($_POST['size'])){
				$size=$_POST["size"];
				$name=str_replace(";","",$_POST["name"]);
				$name=str_replace("..",".",$name);
				$name=str_replace("/","",$name);
				if(is_file($folder.$name)){
					shell_exec("rm -f ".$folder.$name);
					echo "Disk image delete and ";
				}
				shell_exec("qemu-img create ".$folder.$name.".img ".$size."G");
				echo "New disk image create: ".$name.".img<br>Size: ".$size."Gb";
				echo '<meta http-equiv="refresh" content="0; url=./index.php?cmd=listimage" />';
			}else{
				echo "Invalid size";
			}
		}
					exit;
	}
	if($cmd=="delete"){
		if(isset($_POST['name'])){
			$name=str_replace(";","",$_POST["name"]);
			$name=str_replace("..",".",$name);
			$name=str_replace("/","",$name);
			if(is_file($folder.$name.".img")){
				shell_exec("rm -f ".$folder.$name.".img");
				echo "Disk image deleted.";
				echo '<meta http-equiv="refresh" content="0; url=./index.php?cmd=listimage" />';
			}else{
				echo "File not found";
			}
		}
		exit;
	}
if($cmd=="kill"){
		if(isset($_POST['name'])){
			$name=str_replace(";","",$_POST["name"]);
			$name=str_replace("..",".",$name);
			$name=str_replace("/","",$name);
			shell_exec("kill -9 ".$name);
			echo '<meta http-equiv="refresh" content="0; url=./index.php?cmd=list" />';
		}
		exit;
	}
	echo '
<form action="/index.php" method="post" autocomplete="on">
	<TABLE border=1  width="%25">
		<TR>
			<TD>Boot & RAM</TD>
			<TD>Storage</TD>
			<TD>VGA & Ports</TD>
			<TD>Display & Devices</TD>
		</TR>
		<TR>
			<TD>Boot from:
				<br><input type="radio" name="chooseone" value="c">Hda
				<br><input type="radio" name="chooseone" value="d" checked="checked" >Cdrom
				<br><input type="radio" name="chooseone" value="n">Network
				<br><input type="checkbox" name="uefi" value="true">UEFI mode
				<p>RAM:<br><input type="number" name="RAM"  min="0"  style="width: 7em">Mb<br></TD>
			<TD>Hda:
				<br><input type="text" name="hda" value="">
				<br>Hdb:
				<br><input type="text" name="hdb" value="">
				<br>Hdc:
				<br><input type="text" name="hdc" value="">
				<br>Hdd:
				<br><input type="text" name="hdd" value="">
				<br>Cdrom:
				<br><input type="text" name="cdrom" value=""></TD>
			<TD>VGA:
				<br><input type="radio" name="choosetwo" value="null" checked="checked" >Default
				<br><input type="radio" name="choosetwo" value="qxl">QXL
				<br><input type="radio" name="choosetwo" value="cirrus">Cirrus
				<br><input type="radio" name="choosetwo" value="std">Std
				<br><input type="radio" name="choosetwo" value="vmware">VmWare
				<p>Redirect port:(H:G,H:G)<br><input type="text" name="redir"></TD>
			<TD>Display:
				<br><input type="radio" name="choosezero" value="sdl" checked="checked" >SDL
				<br><input type="radio" name="choosezero" value="none">No Display
				<br><input type="radio" name="choosezero" value="vnc" " >VNC:
				<input type="number" name="vnc_port" min="0" value="0" style="width: 7em">
				<p>Devices:
				<br><input type="checkbox" name="mouse" value="true" checked>Usb Mouse
				<br><input type="checkbox" name="keyboard" value="true" checked>Usb Keyboard
				<br><input type="checkbox" name="tablet" value="true" checked>Usb Tablet
				<br><input type="checkbox" name="ac97" value="true" checked>Sound Card(Intel AC97)
				<br><input type="checkbox" name="host_cpu" value="true" checked>Host CPU:<input type="number" name="cores" min="0" max="'.$maxcore.'" value="1" style="width: 7em">
				<p>USB Bus device:(B:A,B:A)<br><input type="text" name="busdev"></TD>
		</TR>
	</TABLE>
	<input type="submit" value="Start Qemu" ><a href="./index.php?cmd=killall">Kill All VM</a>&nbsp&nbsp&nbsp<a href="./index.php?cmd=create">Create New Disk Image</a>&nbsp&nbsp&nbsp<a href="./index.php?cmd=list">List opened VM</a>&nbsp&nbsp&nbsp<a href="./index.php?cmd=listimage">List Disk images</a>
</form>
	';
	if (isset($_POST["RAM"])){
		$a="qemu-system-x86_64 --enable-kvm -net nic -net user";
		if ($_POST["redir"] != ""){
			$redir=$_POST["redir"];
			$redir=",".$redir;
			$redir=str_replace(" ","",$redir);
			$redir=str_replace(":","::",$redir);
			$redir=str_replace(","," -redir tcp:",$redir);
			$a=$a.$redir;
		}	
		if ($_POST["busdev"] != ""){
			$a=$a." -M q35";
			$busdev=$_POST["busdev"];
			$busdev=",".$redir;
			$busdev=str_replace(" ","",$busdev);
			$busdev=str_replace(","," -usbdevice host:",$busdev);
			$a=$a.$busdev;
		}	
		if (is_numeric($_POST["RAM"])){
			$a=$a." -m ".$_POST["RAM"];
		}
		$hda=$_POST["hda"];
		$hda=str_replace("..",".",$hda);
		$hda=str_replace("/","",$hda);
		$hda=$folder.$hda;
		$hdb=$_POST["hdb"];
		$hdb=str_replace("..",".",$hdb);
		$hdb=str_replace("/","",$hdb);
		$hdb=$folder.$hdb;
		$hdc=$_POST["hdc"];
		$hdc=str_replace("..",".",$hdc);
		$hdc=str_replace("/","",$hdc);
		$hdc=$folder.$hdc;
		$hdd=$_POST["hdd"];
		$hdd=str_replace("..",".",$hdd);
		$hdd=str_replace("/","",$hdd);
		$hdd=$folder.$hdd;
		if (is_readable($hda) AND !is_dir($hda)){
			$a=$a." -hda ".$hda;
		}
		if (is_readable($hdb) AND !is_dir($hdb)){
			$a=$a." -hdb ".$hdb;
		}
		if (is_readable($hdc) AND !is_dir($hdc)){
			$a=$a." -hdc ".$hdc;
		}
		if (is_readable($hdd) AND !is_dir($hdd)){
			$a=$a." -hdd ".$hdd;
		}
		$cdrom=$_POST["cdrom"];
		if (is_readable($cdrom) AND !is_dir($cdrom)){
			$a=$a." -cdrom ".$cdrom;
		}
		if (isset($_POST["choosezero"])){
			$a=$a." -display ".$_POST["choosezero"];
			if ($_POST["choosezero"] == "vnc"){
				if(is_numeric($_POST["vnc_port"])){
					$a=$a."=:".$_POST["vnc_port"];
				}
			}
		}
		if (isset($_POST["chooseone"])){
			$a=$a." -boot ".$_POST["chooseone"];
		}
		if (isset($_POST["choosetwo"])){
			if ($_POST["choosetwo"] != "null"){
				$a=$a." -vga ".$_POST["choosetwo"];
			}
		}
		if ($_POST["mouse"] == "true"){
			$a=$a." -usbdevice mouse";
		}
		if ($_POST["uefi"] == "true"){
			$a=$a." -bios /usr/share/ovmf/OVMF.fd";
		}
		if ($_POST["tablet"] == "true"){
			$a=$a." -usbdevice tablet";
		}
		if ($_POST["keyboard"] == "true"){
			$a=$a." -usbdevice keyboard";
		}
		if ($_POST["ac97"] == "true"){
			$a=$a." -soundhw ac97";
		}
		if ($_POST["host_cpu"] == "true"){
			$a=$a." -cpu host";
			if((is_numeric($_POST["cores"]))and ( $maxcore >= $_POST["cores"])){
				$a=$a." -smp cpus=".$_POST["cores"];
			}
			$a=str_replace("&","",$a);
			$a=str_replace("|","",$a);
			$a=str_replace(";","",$a);
			$a=str_replace("$","",$a);
			$a=str_replace("(","",$a);
			$a=str_replace(")","",$a);
			$a=str_replace("\"","",$a);
			$a=str_replace("'","",$a);
			$a=$a." 2> error.txt | : &";
		}
		//echo $a;
		if ($_COOKIE["passwd"] == $passwd){
			echo system($a);
			exit;
		}else{
			echo "<p>Invalid Password:";
			echo $_POST["passwd"]."<br>";
			exit;
		}
	}
}	
else{
	if(isset($_POST['passwd']) AND isset($_POST['user'])){
		session_start();
		setcookie("passwd",$_POST['passwd'],0);
		setcookie("user",$_POST['user'],0);
		}
	echo '
<form action="/index.php" method="post" autocomplete="on">
	User:&nbsp<input type="password" name="user" value="" >
	<br>Password:&nbsp<input type="password" name="passwd" value="" >
	<br><input type="submit" value="Login" >
</form>
	';
	exit;
}
?>
